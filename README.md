Install cmder and chocolatey.

Install everythin else using chocolatey.
```
choco install <software name>
```

Install putty
Edit pageant to pass your ppk key for ssh auth.
Do not set private keyfile for auth only set allow agent forwarding.
If got multiple key for the same site. ensure only one on pagent, because pagent will use first one.


Install git using chocolatey
```
choco install git
```

Configure git to use putty to auth using ssh key.
- set env variable to point to plink.exe.
```
GIT_SSH=C:\Program Files\PuTTY\plink.exe
```

Note:there is bug to add server key. workaround is to use putty to manually ssh in.
Example: open putty and ssh to gitlab.com. login using git.


Install Misc Software
```
choco install filezilla
```
